#!/bin/bash

mailu_front_log=/tmp/mailu_front_1.log
iptables_script=/usr/bin/docker-custom-iptables.sh
blacklist=blacklist

# Redirect the log to a file
docker logs mailu_front_1 >& $mailu_front_log

# Run the banner script to update the blacklist.
./banner.py $1 $mailu_front_log

# Write the IpTables rules using the blacklist.
if [ -e $iptables_script ]; then rm $iptables_script; fi
while read ip; do
  echo "iptables -I DOCKER-USER -s $ip -j DROP" >> $iptables_script
done < $blacklist

# Reload the IpTables rules.
systemctl restart docker-custom-iptables

# Cleanup.
rm -f $mailu_front_log
