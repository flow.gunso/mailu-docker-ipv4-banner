#!/bin/python2

import re
import sys
import argparse
from pprint import pprint


def read_list(file):
  return [line.rstrip() for line in open(file, "rt")]


def write_list(file, list):
  with open(file, "wt") as f:
    f.write("\n".join(list))


class Banner:

  patterns = {
    "http_requests": {
      "line_pattern": r"(?:[0-9]+.?){4} - - \[[0-9]+\/[a-zA-Z]+\/(?:[0-9]+:?){4} \+[0-9]+\] ",
      "ip_pattern": r"(?P<ip>[0-9]+.[0-9]+.[0-9]+.[0-9]+) - - \[[0-9]+\/[a-zA-Z]+\/(?:[0-9]+:?){4} \+[0-9]+\] ",
      "message_pattern": "(?:[0-9]+.?){4} - - \[[0-9]+\/[a-zA-Z]+\/(?:[0-9]+:?){4} \+[0-9]+\] (?P<message>.*)",
    },
    "client_requests": {
      "line_pattern": r"(?:[0-9]+\/?){3} (?:[0-9]+:?){3} \[\w+\] ",
      "ip_pattern": r"client: (?P<ip>[0-9]+.[0-9]+.[0-9]+.[0-9]+)",
      "message_pattern": "(?:[0-9]+\/?){3} (?:[0-9]+:?){3} \[\w+\] (?P<message>.*)",
    },
  }

  def __init__(self, log_file, show_greylisted):
    self.log_file = log_file
    self.blacklist = read_list("blacklist")
    self.whitelist = read_list("whitelist")
    self.greylist = read_list("greylist")
    self.show_greylisted = show_greylisted
    self.queries = {}

  def analyze_log(self):

    # All along the log file.
    with open(self.log_file, "rt") as f:

      # Find the type of line, being an HTTP request or a client request.
      for log_line in f:
        type_of_line = None
        for key in self.patterns:
          if re.search(self.patterns[key]["line_pattern"], log_line) is not None:
            type_of_line = self.patterns[key]
            break
        if type_of_line is None:
          continue

        # Look for an IP in the request.
        ip_search = re.search(type_of_line["ip_pattern"], log_line)
        if ip_search is None:
          continue
        ip = ip_search.group('ip')

        # Skip whitelisted, blacklisted
        if ip in self.blacklist or ip in self.whitelist:
          continue
        # Skip greylisted when not explicitly asked to show them.
        greylisted=False
        if ip in self.greylist:
          greylisted=True
          if not self.show_greylisted:
            continue

        # Add IP and theirs messages to the queries list.
        if ip not in self.queries:
          self.queries[ip] = {}
          self.queries[ip]["messages"] = []
          self.queries[ip]["greylisted"] = greylisted
        message_search = re.search(type_of_line["message_pattern"], log_line)
        message = message_search.group('message')
        self.queries[ip]["messages"].append(message)

  def make_decisions(self):
    for k in self.queries.keys():
      ip = self.queries[k]
      messages = ip["messages"]
      greylisted = ip["greylisted"]
      ip = k

      print("="*30 + " " + ip + " " + "="*30)
      pprint(messages)
      decision = raw_input("Blacklist, Greylist, Whitelist or Ignore? (default: Ignore) [b/g/w/I] ")
      if decision == "b" or decision == "B":
        self.blacklist.append(ip)
      elif decision == "g" or decision == "G":
        self.greylist.append(ip)
      elif decision == "w" or decision == "W":
        self.whitelist.append(ip)
      print("")


  def commit_decisions(self):
    # Remove from the greylist IP that are in the whitelist and the blacklist.
    for ip in self.greylist:
      if ip in self.blacklist or ip in self.whitelist:
        self.greylist.remove(ip)
    
    self.whitelist.sort()
    self.greylist.sort()
    self.blacklist.sort()

    # Write the lists.
    write_list("blacklist", self.blacklist)
    write_list("whitelist", self.whitelist)
    write_list("greylist", self.greylist)

  def execute(self):
    self.analyze_log()
    self.make_decisions()
    self.commit_decisions()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Analyse Mailu's front logs, ban offending IPs.")
  parser.add_argument("-g", "--greylisted", action="store_true", help="Show greylisted IPs")
  parser.add_argument("log_file", help="The log file to analyze")
  args = parser.parse_args()
 
  banner = Banner(log_file=args.log_file, show_greylisted=args.greylisted)
  banner.execute()

