# Mailu Docker IPv4 banner

This set of scripts will allow you to ban IP from querying your Docker networks by analyzing your Mail front log and helping you decide which IP to blacklist, greylist and whitelist.

# Workflow

* A systemd service load a list of iptable rules that drops IPv4 from the DOCKER-USER iptables chain. The service depends on the Docker systemd service, so anytime the latter is reloaded, this on is as well.
* That list is generated with a bash script that reads a blacklist.
* That blacklist is generated with a python script that read the Mailu front logs. The script ask the user to choose, IP per IP, eithe to black/grey/whitelist it, while looking at all the queries that IP did on the front. Whitelisted IP are simply ignored, greylist as well but they can be looked at on demand to change their listing, blacklisted IP are added to the blacklist hence, banned.

# Prequisite

The docker container name of the Mailu front must be mailu_front_1.

# Installation

1. Copy the systemd service file **docker-custom-iptables.service** file to _/usr/lib/systemd/system/_
2. Enable the service file with `systemctl enable docker-custom-iptables.service`

# Usage

Run `banner.sh` to decide which IP to blacklist, greylist or whitelist, from time to time. I do run it once a day to ban around 50 IPs.    
Run `banner.sh -g` to update decisions for greylisted IPs.
